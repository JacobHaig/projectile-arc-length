# Created by Jacob Haig
# 10/23/2018

# This program is use to estimate the Arc Length of
# a projectile with out air resistance

import math
import matplotlib.pyplot as plt # Use for graphing

ACCEL = -9.80665 # This is g
VELOCITY = 100
SMARTMODE = 2 # 1 for WholeArc, 2 for HalfArc
iterations = 6
GRAPH = True
ShowEveryIter = False

startOfRange = 30
endOfRange = 80
NumberOfLines = 10
ACCURACY = 100
ACCURACYUpdateAmount = 2


list = [] # This holds all of our Projectiles

# Define what Points are
class Points:
	x = 0
	y = 0
	def __init__(self, x , y):
		self.x = x
		self.y = y


# Define what Projectile are
class Projectile:
	maxY = 0
	points = []
	initAngle = 0 
	x = 0
	y = 0
	Vx = 0
	Vy = 0
	time = 0

	# x = x + vt + 0.5 * a * t^2
	# 0 = 0 + vt - 0.5 * 9.81 * t^2
	# dx = v + a * t
	# v = 0 + 4.901 * t
	# t = v / 4.901
	# t/2 = v / 4.901 / 2 # Top of arc time
	

	def GetPoint(self, time):
		#self.prints(time) # CAUTION ONLY FOR TESTING
		self.x = self.Vx * time 
		self.y = self.Vy * time + (.5) * ACCEL * time * time 

		if self.y > self.maxY:
			self.maxY = self.y
		
		point = Points(self.x, self.y)
		self.points.append(point)
			
	def __init__(self, angle):
		self.initAngle = angle
		self.points = [Points(0.00000000001, 0.00000000001)]
		self.x = 0.0
		self.y = 0.0
		self.Vx = math.cos(math.radians(angle)) * VELOCITY
		self.Vy = math.sin(math.radians(angle)) * VELOCITY
		self.time = -2 * self.Vy / ACCEL / SMARTMODE # SMARTMODE for only half of the arc #only when drag = 0

	def prints(self, time):
		print(self.initAngle ," Time: ", time ," x: ", self.x ," y: ",self.y, " MaxY: ",self.maxY)



# Get the Distance Between Points
def DisBetween(Points, Points2):
	deltaX = Points2.x - Points.x # Delta X
	deltaY = Points2.y - Points.y # Delta Y
	return math.sqrt(deltaX * deltaX + deltaY * deltaY) # a^2 + b^2 = c^2


# Get the index of the Largest point
def maxId(list):
	id = 0
	maxLength = 0
	for index in range(len(list)):
		if(maxLength < list[index]):
			maxLength = list[index]
			id = index
	return id
	

def run(iteration):
	# Create the projectile
	Scale = 1000000000
	START = int(startOfRange * Scale)
	END = int(endOfRange * Scale)
	STEP = int((endOfRange - startOfRange) / NumberOfLines * Scale)
	for Angle in range(START, END, STEP): # Parameters numbers must be intergers
		list.append(Projectile(Angle / Scale)) 
	

	# Create The Path
	for projectile in list:
		for x in range(0, ACCURACY + 1):
			time = projectile.time * (x / ACCURACY)
			projectile.GetPoint(time)
		#print("Done ", projectile.initAngle)
	print("")


	# Get Distance of line
	disList = []
	for projectile in list:
		sum = 0
		for index in range(len(projectile.points) - 1):
			sum = sum + DisBetween(projectile.points[index], projectile.points[index + 1])
		print("Angle: ", projectile.initAngle, " = " , sum * SMARTMODE) # SMARTMODE for only half of the arc #only when drag = 0
		disList.append(sum)
	
	
	if GRAPH and iterations == iteration + 1 or ShowEveryIter:
		# Create the Lines
		for projectile in list:
			pointListX = []
			pointListY = []
			for point in projectile.points:
				pointListX.append(point.x)
				pointListY.append(point.y)
			plt.plot(pointListX, pointListY)

		# Display Graph
		plt.grid()
		plt.title("Arc of Projectile")
		plt.ylabel('Height')
		plt.xlabel('Distance')
		plt.show()


	# Prepare for NEXT iteration
	return list[maxId(disList)].initAngle

for i in range(iterations):
	BiggestAngle = run(i)
	print(startOfRange," ",BiggestAngle," ",endOfRange," ",ACCURACY)
	startOfRange = -(BiggestAngle - startOfRange) / 2 + BiggestAngle
	endOfRange = (endOfRange - BiggestAngle) / 2 + BiggestAngle
	ACCURACY = ACCURACY * ACCURACYUpdateAmount
	list = []
		